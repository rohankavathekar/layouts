//navbar positions
$(window).scroll(function() {
  'use strict';
  var scroll = $(window).scrollTop();
  if (scroll >= 50) {
    $(".navbar").addClass("newbar");
  }
  else {
    $(".navbar").removeClass("newbar");
  }
});

//Typed Words
var typed = new Typed('.typed-words', {
  strings: ["Businesses"," Services"],
  typeSpeed: 80,
  backSpeed: 80,
  backDelay: 4000,
  startDelay: 1000,
  loop: true,
  showCursor: true
});


//carousel
$(document).ready(function(){
  'use strict';
  $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    responsive:{
      0:{
          items:1,
          nav:true
      },
      480:{
          items:2,
          nav:false
      },
      768:{
          items:5,
          nav:false
      },
      1024:{
          items:7,
          nav:true,
          loop:true
      }
    }
  });

  $( ".owl-next").html('<img src="images/next.png"/>');
  $( ".owl-prev").html('<img src="images/prev.png"/>');

  // this custom function is written for removing active tag
  $('#pills-tab a').on('click', function(e) {
    e.preventDefault();
    $(this).tab('show');
    var theThis = $(this);
    $('#pills-tab a').removeClass('active');
    theThis.addClass('active');
    });
  });

//parallax effect
// $.stellar();





